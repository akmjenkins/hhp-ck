<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#">About Dr. Ravi</a></li>
										<li><a href="#" class="selected">Credentials</a></li>
										<li><a href="#">Regulations</a></li>
										<li><a href="#">Ask A Question</a></li>
										<li><a href="#">Success Stories</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Dr. Ravi</a>
									<a href="#">Credentials</a>
								</div>
								
							</div><!-- .sec-nav -->
								
							<div class="article-head">
								<div class="hgroup">
									<h2>The Latest</h2>
									<span class="subtitle">Tellus sed arcu ultrices ornare in. </span>
								</div>
							</div><!-- .article-head -->
							
							<div class="the-latest-slider swiper-wrapper">
								
								<div class="swiper" data-responsive='<?php echo json_encode(array(array('breakpoint'=>'850','settings'=>'unslick'))); ?>'>
								
									<div class="swipe-item">
									
										<div class="grid eqh blocks collapse-at-850 nopad">
										
											<div class="col-2 col">
												<div class="item pad-40 sm-pad-10">
												
													<a class="block with-button" href="#">
														<div class="img-wrap ar" data-ar="50">
															<div class="img lazybg" data-src="../assets/images/temp/latest/10.jpg"></div>
														</div><!-- .img-wrap -->
														<div class="content">
														
															<div class="title-block">
																<span class="h4-style heading title">This is a News Article</span>
																<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
															</div>
															<time class="meta" datetime="2014-10-14">October 14, 2014</time>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<span class="button big">Read More</span>
															
														</div><!-- .content -->
													</a><!-- .block -->
													
												</div><!-- .item -->
											</div><!-- .col -->

											<div class="col-2 col">
												<div class="item pad-40 sm-pad-10">
												
													<a class="block with-button" href="#">
														<div class="img-wrap ar" data-ar="50">
															<div class="img lazybg" data-src="../assets/images/temp/latest/10.jpg"></div>
														</div><!-- .img-wrap -->
														<div class="content">
														
															<div class="title-block">
																<span class="h4-style heading title">This is a News Article</span>
																<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
															</div>
															<time class="meta" datetime="2014-10-14">October 14, 2014</time>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<span class="button big">Read More</span>
															
														</div><!-- .content -->
													</a><!-- .block -->
													
												</div><!-- .item -->
											</div><!-- .col -->
											
										</div><!-- .grid -->
									
									</div><!-- .swipe-item -->

									<div class="swipe-item">
									
										<div class="grid eqh blocks collapse-at-850 nopad">
										
											<div class="col-2 col">
												<div class="item pad-40 sm-pad-10">
												
													<a class="block with-button" href="#">
														<div class="img-wrap ar" data-ar="50">
															<div class="img lazybg" data-src="../assets/images/temp/latest/10.jpg"></div>
														</div><!-- .img-wrap -->
														<div class="content">
														
															<div class="title-block">
																<span class="h4-style heading title">This is a News Article</span>
																<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
															</div>
															<time class="meta" datetime="2014-10-14">October 14, 2014</time>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<span class="button big">Read More</span>
															
														</div><!-- .content -->
													</a><!-- .block -->
													
												</div><!-- .item -->
											</div><!-- .col -->

											<div class="col-2 col">
												<div class="item pad-40 sm-pad-10">
												
													<a class="block with-button" href="#">
														<div class="img-wrap ar" data-ar="50">
															<div class="img lazybg" data-src="../assets/images/temp/latest/10.jpg"></div>
														</div><!-- .img-wrap -->
														<div class="content">
														
															<div class="title-block">
																<span class="h4-style heading title">This is a News Article</span>
																<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
															</div>
															<time class="meta" datetime="2014-10-14">October 14, 2014</time>
															
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
															Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
															
															<span class="button big">Read More</span>
															
														</div><!-- .content -->
													</a><!-- .block -->
													
												</div><!-- .item -->
											</div><!-- .col -->
											
										</div><!-- .grid -->
									
									</div><!-- .swipe-item -->
									
								</div><!-- .swiper -->
								
							</div><!-- .the-latest-slider -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	
	<section class="dark-bg blue-bg">
		<div class="sw">
		
			<div class="article-head">
				<div class="hgroup nosep">
					<h3>Testimonial</h3>
					<span class="subtitle">Lorem ipsum dolor sit amet</span>
				</div>
			</div><!-- .article-head -->
			
			<div class="testimonial-single grid eqh vcenter fill">
			
				<div class="col col-2">
					<div class="item">
						<div class="item-content">
							<h4>Janice Wells</h4>
							<p>
								There are not many things of high importance in life but family and health are. 
								I am so grateful that Dr. Ravi has been helping us during times of challenge with professionalism, 
								compassion and caring. We have been his patients for six years.
							</p>
							<a href="#" class="button">Read</a>
							<a href="#" class="button">View All</a>
						</div><!-- .item-content -->
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 xs-no">
					<div class="item">
						<div class="testimonial-single-img-wrap lazybg">
							<img src="../assets/images/temp/janice.jpg" alt="janice">
						</div>
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .testimonial-single -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	

	<section>
		<div class="sw">
			<div class="article-head">
				<div class="hgroup nosep">
					<h4>Events</h4>
					<span class="subtitle">Lorem ipsum dolor sit amet</span>
				</div>
			</div><!-- .article-head -->
		</div><!-- .sw -->
		
		<div class="filter-area">
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">8</span> Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-area -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid eqh blocks collapse-at-850">
					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="../assets/images/temp/latest/7.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event One</span>
											<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="../assets/images/temp/latest/8.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event Two</span>
											<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="../assets/images/temp/latest/9.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event Three</span>
											<span class="h5-style heading subtitle">Lorem ipsum dolor sit amet</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				
				</div><!-- .sw -->
			</div><!-- .filter-content -->
		</div><!-- .filter-area -->
	</section>
	
	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>

	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
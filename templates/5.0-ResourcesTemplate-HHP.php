<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
							
								<div class="sec-nav">
								
									<div class="sec-nav-links">
										<button class="nav-button t-fa-abs fa-navicon">Menu</button>
										<ul>
											<li><a href="#">Resources</a></li>
										</ul>
									</div><!-- .sec-nav-links -->
								
									<div class="breadcrumbs">
										<a href="#">Resources</a>
									</div>
									
								</div><!-- .sec-nav -->
									
								<div class="article-head">
									<div class="hgroup">
										<h2>Resources</h2>
										<span class="subtitle">Tellus sed arcu ultrices ornare in.</span>
									</div>
								</div><!-- .article-head -->
		
						</div><!-- .item-content -->
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="filter-area">
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">7</span>
						Resources Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-area -->
			
			<div class="filter-content">
				<div class="sw">
					<div class="grid eqh fill vcenter round-blocks">
						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource One</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">PDF</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource Two</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">Web</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->

						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource Three with a little extra text</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">Doc</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource Four</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">Read</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource Five</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">Read</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->
						
						<div class="col col-3 sm-col-2 xs-col-1">
							<a class="item dark-bg blue-bg abs-button" href="#">
							
								<div class="pad-20 center">
									<h4>Resource Six</h4>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									</p>
									
									<span class="btn-wrap">
										<span class="button big white uc">Read</span>
									</span><!-- .btn-wrap -->
								</div><!-- .pad-20 -->
								
							</a><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				</div><!-- .sw -->
			</div><!-- .filter-content -->
		</div><!-- .filter-area -->
	</section>
	
	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
<div class="hero has-lightslider ls-bar-links">

	<ul class="lightslider" data-links="true" data-auto="true" data-pause="5000">
	
		<li data-src="../assets/images/temp/hero/hero-1.jpg">
			<div class="item">&nbsp;</div>
			<div class="sw">
				<div class="hero-caption left">
				
					<div class="hero-caption-title">Now Accepting Online Consultations</div>
					
					<p>
						You can get effective homeopathic treatment without leaving your home or office.
					</p>
					
				</div><!-- .caption -->
			</div><!-- .sw -->
		</li>
		
		<li data-src="../assets/images/temp/hero/hero-2.jpg">
			<div class="item">&nbsp;</div>
			<div class="sw">
				<div class="hero-caption right">
				
					<div class="hero-caption-title">Now Accepting Online Consultations</div>
					
					<p>
						You can get effective homeopathic treatment without leaving your home or office.
					</p>
					
				</div><!-- .caption -->
			</div><!-- .sw -->
		</li>
		
		<li data-src="../assets/images/temp/hero/hero-3.jpg" class="dark-bg">
			<div class="item">&nbsp;</div>
			<div class="sw">
				<div class="hero-caption left">
				
					<div class="hero-caption-title">Now Accepting Online Consultations</div>
					
					<p>
						You can get effective homeopathic treatment without leaving your home or office.
					</p>
					
				</div><!-- .caption -->
			</div><!-- .sw -->
		</li>
		
	</ul>
	
</div><!-- .hero -->
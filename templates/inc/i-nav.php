<button class="toggle-nav">
	<span></span>
</button>
<div class="mobile-nav-bg"></div>
<div class="nav-wrap">

	<div class="nav">
		<div class="nav-contents">
			<div class="sw">
				<nav>
					<ul>
						<li class="drop selected">
							<a href="#">
								<span>Dr. Ravi</span>
							</a>
							<div class="nav-dd">
							
								<ul>
									<li><a href="#">About Dr. Ravi</a></li>
									<li><a href="#" class="selected">Credentials</a></li>
									<li><a href="#">Regulations</a></li>
									<li><a href="#">Ask a Question</a></li>
									<li><a href="#">Success Stories</a></li>
								</ul>
								
								<div class="nav-dd-meta lazybg" data-src="../assets/images/temp/dd.jpg">
									<span class="nav-dd-meta-title">Homeopathy is...</span>
									<span class="nav-dd-meta-content">a very effective, natural, and complete system of medicine</span>
								</div>
								
							</div>
						</li>
						<li><a href="#"><span>Homeopathy</span></a></li>
						<li><a href="#"><span>Conditions Treated</span></a></li>
						<li><a href="#"><span>Services</span></a></li>
						<li><a href="#"><span>Book Consultation</span></a></li>
					</ul>
				</nav>
			</div><!-- .sw -->
			
			<div class="nav-top">
				<div class="sw">
				
					<button class="toggle-search t-fa-abs fa-search">Search</button>
				
					<div class="nav-meta-nav">
						<a href="#"><span data-t="Sign In">Sign In</span></a>
						<a href="#"><span data-t="Resources">Resources</span></a>
						<a href="#"><span data-t="The Latest">The Latest</span></a>
						<a href="#"><span data-t="Contact Us">Contact Us</span></a>
					</div><!-- .meta-nav -->

				</div><!-- .sw -->
			</div><!-- .nav-top -->
		</div><!-- .nav-contents -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->
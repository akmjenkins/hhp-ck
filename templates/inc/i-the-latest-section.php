		<div class="the-latest-section">
			<div class="grid fill nopad eqh vcenter">
			
				<div class="col col-2 sm-col-1">
					<div class="item">
						<div class="pad-20">
							<h3>Latest News &amp; Resources</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
							<a href="#" class="button">Read More</a>
						</div>					
					</div><!-- .item -->
				</div><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/1.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<!-- this will ensure a minimum aspect ratio of 80 is maintained in case there isn't enough content in Latest News &amp; Resources -->
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/2.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/3.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/4.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/5.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<!-- this will ensure a minimum aspect ratio of 80 is maintained in case there isn't enough content in Latest News &amp; Resources -->
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
				<a href="#" class="col col-4 sm-col-2 xs-col-1">
					<div class="item lazybg" href="#" data-src="../assets/images/temp/latest/6.jpg">
						<div class="c">
							<span class="t">Vestibulum non Suscipit Erat Lorem</span>
							<span class="button white p square">Conditions</span>
						</div>
							
						<div class="ar" data-ar="80" ></div>
					</div><!-- .item -->
				</a><!-- .col -->
				
			</div><!-- .grid -->
		</div><!-- .the-latest-section -->

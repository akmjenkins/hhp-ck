<h4>Circulatory Conditions</h4>

<div class="condition-list">
	<ul>
		<li>
			<span>Varicose Veins</span>
			<a href="#" class="button">View</a>
		</li>
		<li>
			<span>Piles</span>
			<a href="#" class="button">View</a>
		</li>
		<li>
			<span>Cold Hands & feet</span>
			<a href="#" class="button">View</a>
		</li>
		<li>
			<span>Hypertension</span>
			<a href="#" class="button">View</a>
		</li>
		<li>
			<span>Angina</span>
			<a href="#" class="button">View</a>
		</li>
		<li>
			<span>Heart Failure</span>
			<a href="#" class="button">View</a>
		</li>
	</ul>
</div><!-- .condition-list -->

<div class="conditions-book dark-bg blue-bg">
	<span class="count">
		<strong>0</strong>
		<span>Conditions Selected</span>
	</span>
	<a href="#" class="button">
		Book Consultation
	</a>
</div><!-- .conditions-book -->
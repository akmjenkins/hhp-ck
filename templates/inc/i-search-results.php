<div class="results-list tab-wrapper">
	
	<div class="results-query">
		16 Results for for "Search Query".
	</div><!-- .results-query -->

	<div class="results-types tab-controls">

			<div class="selector with-arrow">
				<select class="tab-controller">
					<option selected>News (10 Results)</option>
					<option>Pages (3 Results)</option>
					<option>Promotions (3 Results)</option>
				</select>
				<span class="value">&nbsp;</span>
			</div><!-- .selector -->

			<div class="tab-control-wrap">
				<div class="selected tab-control">
					<span>10</span>
					news
				</div>
				
				<div class="tab-control">
					<span>3</span>
					pages
				</div>
				
				<div class="tab-control">
					<span>3</span>
					promotions
				</div>	
			</div><!-- .tab-control-wrap -->	
			
	</div><!-- .results-types -->
	
	<div class="results tab-holder scroll-fix">
	
		<div class="tab selected">
			<?php include('i-sample-results-list.php'); ?>
		</div><!-- .tab -->

		<div class="tab">
			<?php include('i-sample-results-list.php'); ?>
		</div><!-- .tab -->

		<div class="tab">
			<?php include('i-sample-results-list.php'); ?>
		</div><!-- .tab -->
	
	</div><!-- .results -->
	
</div><!-- .results-list -->
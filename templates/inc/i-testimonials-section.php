			<div class="testimonials-section">
				<div class="grid eqh vcenter nopad fill">
					<div class="col col-2">
						<div class="item has-lightslider ls-button-links">
							<div class="pad-20 sm-pad-10 xs-pad-5">
								<h3>What Our Patients Say...</h3>
								<ul class="lightslider" data-mode="slide" data-links="true">
									<li>
										<blockquote>
											There are not many things of high importance in life but family and health are. I am so grateful that Dr. Ravi 
											has been helping us during times of challenge with professionalism, compassion and caring. We have been his patients for six years.
											<cite>&mdash; Janice Wells</cite>
										</blockquote>						
									</li>
									<li>
										<blockquote>
											There are not many things of high importance in life but family and health are. I am so grateful that Dr. Ravi 
											has been helping us during times of challenge with professionalism, compassion and caring. We have been his patients for six years.
											<cite>&mdash; Janice Wells</cite>
										</blockquote>						
									</li>
									<li>
										<blockquote>
											There are not many things of high importance in life but family and health are. I am so grateful that Dr. Ravi 
											has been helping us during times of challenge with professionalism, compassion and caring. We have been his patients for six years.
											<cite>&mdash; Janice Wells</cite>
										</blockquote>						
									</li>
									<li>
										<blockquote>
											There are not many things of high importance in life but family and health are. I am so grateful that Dr. Ravi 
											has been helping us during times of challenge with professionalism, compassion and caring. We have been his patients for six years.
											<cite>&mdash; Janice Wells</cite>
										</blockquote>						
									</li>
								</ul><!-- .lightslider -->
							</div><!-- .pad20 -->
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2 no-sm">
						<div class="item lazybg ov-img" data-src="../assets/images/temp/full.png">
							<div class="ar" data-ar="70"></div>
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .testimonials-section -->

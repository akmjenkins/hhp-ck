			<footer>
				<div class="footer-wrap">
					<div class="sw">
						
						<div class="footer-block">
							
							<h4>About Dr. Ravi</h4>
							<p>My experience in Homeopathy goes back to my personal experience with a Homeopath as a teenager. 
							As I grew up my father encouraged and supported my choice to join a Homeopathic Medical College. 
							This was the beginning of my learning experience.</p>
							<a href="#" class="button">More</a>
							
						</div><!-- .footer-block -->
						
						<div class="footer-block">
							
							<div class="footer-nav">
								<ul>
									<li><a href="#">Dr. Ravi</a></li>
									<li><a href="#">Homeopathy</a></li>
									<li><a href="#">Conditions Treated</a></li>
									<li><a href="#">Services</a></li>
									<li><a href="#">Contact</a></li>
								</ul>
							</div><!-- .footer-nav -->
							
							<div class="s-footer-blocks">
							
								<div class="s-footer-block">
								
									<h6>Location</h6>
									
									<address>
										15 Penny Lane, West Main St.<br />
										Moncton NB, E1E 4W4
									</address>
									
									<div class="rows">
										<span class="row">
											<span class="l">Phone:</span>
											<span class="r">506 855 2230</span>
										</span>
										
										<span class="row">
											<span class="l">Fax:</span>
											<span class="r">506 855 2230</span>
										</span>
									</div><!-- .rows -->
									
								</div><!-- .s-footer-block -->
								
								<div class="s-footer-block">
								
									<h6>Office Hours</h6>
									
									Monday 8:00am - 7:30pm <br />
									Tuesday 8:00am - 5:00pm <br />
									Wednesday 8:00am - 5:00pm <br />
									Thursday 8:00am - 5:00pm <br />
									Friday 8:00am - 1:00pm
									
								</div><!-- .s-footer-block -->
								
								<div class="s-footer-block">
								
									<?php include('i-social.php'); ?>
									
								</div><!-- .s-footer-block -->
								
							</div><!-- .s-footer-blocks -->
							
						</div><!-- .footer-block -->
						
						
					</div><!-- .sw -->
				</div><!-- .footer-wrap -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Homeopathy Health Plus</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<?php include('inc/i-search-overlay.php'); ?>
			
		<script>
			var templateJS = {
				templateURL: 'http://127.0.0.1/hhp-ck',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>
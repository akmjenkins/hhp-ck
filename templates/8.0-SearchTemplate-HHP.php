<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#" class="selected">Search Results</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Search Results</a>
								</div>
								
							</div><!-- .sec-nav -->

							<div class="article-head">
								<div class="hgroup">
									<h2>Search Results</h2>
									<span class="subtitle">Tellus sed arcu ultrices ornare in. </span>
								</div>
							</div><!-- .article-head -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
			<div class="article-head">
				<div class="hgroup nosep">
					<h4>News Results</h4>
					<span class="subtitle">Tellus sed arcu ultrices ornare in. </span>
				</div>
			</div><!-- .article-head -->
		</div><!-- .sw -->
		
		<div class="filter-area">
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">8</span> Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-area -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid eqh blocks collapse-at-850">
					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">News Result One</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">News Result Two</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">News Result Three</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				
				</div><!-- .sw -->
			</div><!-- .filter-content -->
		</div><!-- .filter-area -->
	</section>

	<section>
		<div class="sw">
			<div class="article-head">
				<div class="hgroup nosep">
					<h4>Event Results</h4>
					<span class="subtitle">Tellus sed arcu ultrices ornare in. </span>
				</div>
			</div><!-- .article-head -->
		</div><!-- .sw -->
		
		<div class="filter-area">
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-controls">
						<button class="previous">Prev</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->
				
					<div class="count">
						<span class="num">8</span> Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-area -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid eqh blocks collapse-at-850">
					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event One</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event Two</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Event Three</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
											<time class="meta" datetime="2014-11-09">November 9, 2014</time>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				
				</div><!-- .sw -->
			</div><!-- .filter-content -->
		</div><!-- .filter-area -->
	</section>

	<section>
		<div class="sw">
			<div class="article-head">
				<div class="hgroup nosep">
					<h4>Page Results</h4>
					<span class="subtitle">Tellus sed arcu ultrices ornare in. </span>
				</div>
			</div><!-- .article-head -->
		</div><!-- .sw -->
		
		<div class="filter-area">
			<div class="filter-bar">
				<div class="sw">
				
					<div class="count">
						<span class="num">2</span> Found
					</div><!-- .count -->
					
				</div><!-- .sw -->
			</div><!-- .filter-area -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid eqh blocks collapse-at-850">
					
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Page One</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item">
							
								<a class="block with-meta" href="#">
									<div class="img-wrap ar" data-ar="32">
										<div class="img lazybg" data-src="http://adamjenkins.s463.sureserver.com/weath../assets/images/temp/overview/ov4.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="title-block">
											<span class="h4-style heading title">Page Two</span>
											<span class="h5-style heading subtitle">Sub Title</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
										Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
										
										
										<div class="bottom-meta">
											<span class="button">Read More</span>
										</div><!-- .bottom-meta -->
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

					</div><!-- .grid -->
				
				</div><!-- .sw -->
			</div><!-- .filter-content -->
		</div><!-- .filter-area -->
	</section>

	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
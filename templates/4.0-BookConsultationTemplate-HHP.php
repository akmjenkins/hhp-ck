<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#" class="selected">Book a Consultation</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Book a Consultation</a>
								</div>
								
							</div><!-- .sec-nav -->

							<div class="article-head">
								<div class="hgroup">
									<h2>Book a Consultation</h2>
									<span class="subtitle">Tellus sed arcu ultrices ornare in.</span>
								</div>
							</div><!-- .article-head -->
								
								<div class="main-body">
									<div class="content article-body">
									
										<div class="grid fill pad40">
											<div class="col col-2 sm-col-1">
												<div class="item">
												
													<p>
														Sed quam nunc, posuere sed ante vitae, semper imperdiet sem. Cras vulputate id metus eget luctus. 
														Nullam condimentum porttitor dictum. Cras vehicula orci id leo placerat blandit. In hac habitasse platea dictumst.
													</p>

													<p>
														Duis placerat congue mauris et vehicula. Sed ultrices tellus id sapien blandit rutrum. Suspendisse vitae lacus feugiat, 
														condimentum leo non, congue lorem. Sed et orci vulputate, feugiat erat eu, laoreet magna. 
														Quisque facilisis quam urna, sed tincidunt tortor vestibulum eu. Nullam a orci non augue pulvinar blandit. 
														Pellentesque lacinia lectus eget dolor imperdiet, et accumsan arcu porttitor. 
														Donec porta semper urna vitae consequat.
													</p>

													<p>
														Aenean at interdum nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
														Proin pretium lacus eget sapien tempus, a elementum eros pretium. Donec iaculis velit et neque lobortis scelerisque. 
														Morbi ultricies nulla arcu, id porttitor quam ornare non. Quisque fringilla sem sed ligula feugiat efficitur ac ac arcu. 
														In consequat ipsum non dictum dapibus.
													</p>
													
												</div><!-- .item -->
											</div><!-- .col -->
											<div class="col col-2 sm-col-1">
												<div class="item">
												
													<form action="/" method="post" class="body-form dark-fields">
														<fieldset>
											
															<span class="field-wrap t-fa-abs fa-user">
																<input type="text" name="name" placeholder="Name">
															</span><!-- .field-wrap -->
															
															<span class="field-wrap t-fa-abs fa-lock">
																<input type="email" name="email" placeholder="E-mail Address">
															</span><!-- .field-wrap -->
															
															<span class="field-wrap t-fa-abs fa-pencil ta-wrap">
																<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
															</span>
															
															<button type="submit" class="button big">Submit</button>
															
														
														</fieldset>
													</form><!-- .body-form -->
												
												</div><!-- .item -->
											</div><!-- .col -->
										</div><!-- .grid -->
						
									</div><!-- .content -->
								</div><!-- .main-body -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg green-bg">
		<div class="sw">
			
			<div class="section-header">
				<h3>Book Consultation Online</h3>
				<span class="subtitle">You can get effective homeopathic treatment without leaving your home or office.</span>
			</div><!-- .section-header -->
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="light-green-bg nopad">
		<div class="sw">
		
			<div class="grid eqh ov-blocks nopad">
				<div class="col col-2 sm-col-1">
					<div class="item ov-block">
					
						<div class="body-form-wrap pad-20 sm-pad-10">
							<h3>Log In</h3>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
							
							<form action="/" class="body-form" method="post">
								<fieldset>
									
									<span class="field-wrap t-fa-abs fa-user">
										<input type="email" name="email" placeholder="E-mail">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa-abs fa-lock">
										<input type="password" name="password" placeholder="Password">
									</span><!-- .field-wrap -->
									
									<button type="submit" class="button big">Log In</button>
									
									<span class="block f-right alright">
										<p>Did you forget your password?</p>
										<a href="#" class="right inline">Forgot password?</a>
									</span><!-- .block -->
									
								</fieldset>
							</form><!-- .body-form -->
						</div><!-- .pad-20 -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 sm-col-1">
					<div class="item ov-block">
					
						<div class="body-form-wrap pad-20 sm-pad-10">
							<h3>Sign Up</h3>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
							
							<form action="/" class="body-form" method="post">
								<fieldset>
								
									<span class="field-wrap t-fa-abs fa-user">
										<input type="text" name="name" placeholder="Full Name">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa-abs fa-envelope">
										<input type="email" name="email" placeholder="E-mail">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa-abs fa-lock">
										<input type="password" name="password" placeholder="Password">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa-abs fa-lock">
										<input type="password" name="cpassword" placeholder="Confirm Password">
									</span><!-- .field-wrap -->
									
									<button type="submit" class="button big">Sign Up</button>
									
								</fieldset>
							</form><!-- .body-form -->
						</div><!-- .pad-20 -->
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section><!-- .light-green-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
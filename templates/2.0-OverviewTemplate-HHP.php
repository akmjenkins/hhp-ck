<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh vcenter nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="hgroup">
								<h2>About Dr. Ravi</h2>
								<span class="subtitle">Professional, Compassionate and caring.</span>
							</div>
						
							<p>
								There are not many things of high importance in life but family and health are. I am so grateful that Dr. Ravi has been helping us during times of challenge 
								with professionalism, compassion and caring. We have been his patients for six years.
							</p>
						
							<a href="#" class="big button">Read More</a>
	
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 no-sm">
					<div class="item lazybg ov-img" data-src="../assets/images/temp/full.png">
						<div class="ar" data-ar="80"></div>
					</div><!-- .item -->
				</div>
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>

	<section class="nopad light-green-bg">
			
			<div class="grid nopad eqh vcenter ov-blocks">
				<div class="col col-2 sm-col-1">
					<a class="item ov-block" href="#">
						
						<div class="ov-block-content">
							<div class="hgroup">
								<h3>Credentials</h3>
								<span class="subtitle">Cras convallis orci a erat aliquet luctus</span>
							</div>
							
							<p>Vivamus ultricies malesuada ante rhoncus sollicitudin. Donec tincidunt iaculis aliquam. Sed ornare, 
							massa quis iaculis ullamcorper, diam arcu auctor est, in ornare enim</p>
							<span class="button">Read More</span>
						</div>
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 sm-col-1">
					<a class="item ov-block" href="#">
						
						<div class="ov-block-content">
							<div class="hgroup">
								<h3>Regulations</h3>
								<span class="subtitle">Cras convallis orci </span>
							</div>
							
							<p>Vivamus ultricies malesuada ante rhoncus sollicitudin.</p>
							<span class="button">Read More</span>
						</div>
						
					</a><!-- .item -->
				</div><!-- .col -->
				<hr />
				<div class="col col-2 sm-col-1">
					<a class="item ov-block" href="#">
						
						<div class="ov-block-content">
							<div class="hgroup">
								<h3>Ask a Question</h3>
								<span class="subtitle">Cras convallis orci </span>
							</div>
							
							<p>Vivamus ultricies malesuada ante rhoncus sollicitudin.</p>
							<span class="button">Read More</span>
						</div>
						
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col col-2 sm-col-1">
					<a class="item ov-block" href="#">
						
						<div class="ov-block-content">
							<div class="hgroup">
								<h3>Success Stories</h3>
								<span class="subtitle">Cras convallis orci a erat aliquet luctus</span>
							</div>
							
							<p>Vivamus ultricies malesuada ante rhoncus sollicitudin. Donec tincidunt iaculis aliquam. Sed ornare, 
							massa quis iaculis ullamcorper, diam arcu auctor est, in ornare enim</p>
							<span class="button">Read More</span>
						</div>
						
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
	</section><!-- .light-green-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
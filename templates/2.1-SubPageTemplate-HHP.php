<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#" class="selected">About Dr. Ravi</a></li>
										<li><a href="#">Credentials</a></li>
										<li><a href="#">Regulations</a></li>
										<li><a href="#">Ask A Question</a></li>
										<li><a href="#">Success Stories</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Dr. Ravi</a>
									<a href="#">About Dr. Ravi</a>
								</div>
								
							</div><!-- .sec-nav -->

							<div class="article-head">
								<div class="hgroup">
									<h2>About Dr. Ravi</h2>
									<span class="subtitle">Professional, Compassionate and caring.</span>
								</div>
							</div><!-- .article-head -->
							
							<div class="main-body">
								<div class="content article-body">
						
									<p>My experience in Homeopathy goes back to my personal experience with a Homeopath as a teenager. 
									My parents made this introduction to the exciting world of homeopathy. As I grew up my father encouraged 
									and supported my choice to join a Homeopathic Medical College. This was the beginning of my 
									learning experience. My passion in this field only grew as I got the chance to work in a hospital, 
									and was also exposed to wonderful teachers.</p>
		 
									<p>I will be forever grateful to a number of Homeopathic Doctors that shared their knowledge and wisdom 
									with me. To obtain my degree, Bachelor of Homeopathic Medicine and Surgery (B.H.M.S.) I studied four 
									and a half years of medical science with homeopathic medicine and completed one year of homeopathic 
									residency in 1994. In India I was given the opportunity to understand the medical system as a resident 
									medical officer in a few hospitals with different specialists. After receiving my medical degree (B.H.M.S.).</p>
		 
									<p>I not only was able to work full time in a homeopathic clinic and wellness center but I also worked as an 
									assistant professor for two years. My work with charity hospitals allowed me to successfully visit 10 villages and 
									help a vast number of patients with various conditions. In 1999 I moved to Canada which led to my opening a clinic 
									in Moncton, New Brunswick. My practice has since expanded to include Fredericton, New Brunswick and Toronto, 
									Ontario. With the use of the Internet and telephone I now have clients from around the world. My practice touches 
									clients in different provinces within Canada, the USA, England, India, Russia.</p>
							
								</div><!-- .content -->
							</div><!-- .main-body -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2 no-sm">
					<div class="item lazybg ov-img" data-src="../assets/images/temp/full.png">
						<div class="ar" data-ar="80"></div>
					</div><!-- .item -->
				</div>
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>

	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
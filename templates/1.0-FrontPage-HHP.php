<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-hero-home.php'); ?>

<div class="body">

	<section class="blue-bg dark-bg">
		<div class="sw">
			<?php include('inc/i-book-appointment-section.php'); ?>
		</div><!-- .sw -->
	</section><!-- .blue-bg -->
	
	<section class="light-green-bg nopad">
		<div class="sw">
			<?php include('inc/i-testimonials-section.php'); ?>
		</div><!-- .sw -->
	</section><!-- .light-green-bg -->

	<section class="nopad">	
		<div class="sw full">
			<?php include('inc/i-the-latest-section.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
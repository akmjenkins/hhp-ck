var ns = 'JAC-HHP';
window[ns] = {};

// jQuery
// @codekit-prepend "../../bower_components/jquery/dist/jquery.min.js";

// magnific popup
// @codekit-prepend "../../bower_components/magnific-popup/dist/jquery.magnific-popup.min.js";

// lightslider
// @codekit-append "scripts/jquery.lightslider.js"

// debounce - used by a bunch of things
// @codekit-append "scripts/debounce.js";

// my lightslider init script - used in hero.js
// @codekit-append "scripts/lightslider.js"

// slickjs
// @codekit-prepend "../../bower_components/slick.js/slick/slick.js
// @codekit-append "scripts/swiper.js"

// @codekit-append "scripts/anchors.external.popup.js"; 
// @codekit-append "scripts/standard.accordion.js";
// @codekit-append "scripts/magnific.popup.js";
// @codekit-append "scripts/responsive.video.js";
// @codekit-append "scripts/tabs.js";
// @codekit-append "scripts/blocks.js";

// @codekit-append "scripts/aspect.ratio.js";
// @codekit-append "scripts/lazy.images.js";

// @codekit-append "scripts/search.js
// @codekit-append "scripts/nav.js
// @codekit-append "scripts/hero.js
// @codekit-append "scripts/conditions.js

(function($,context) {

}(jQuery,window[ns]));
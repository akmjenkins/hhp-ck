(function(context) {

	var 
		lightslider = context.lightslider,
		slides,
		el = $('div.hero'),
		methods = {
			loadActive: function() {
				var 
					self = this,
					active = this.getActiveSlide(),
					img = active.data('src');
					
				//load the img
				this.isWorking(active) || $('<img/>')
					.on('load',function() {
						self.updateSlides(self.getSlides().filter(function() { return $(this).data('src') === img; }),img);
					})
					.attr('src',img);
					
				active.addClass('loading');
				
			},
			
			updateSlides: function(els,img) {
			
				els
					.find('div.item')
					.css({
						backgroundImage: 'url('+img+')'
					})
					.end()
					.addClass('loaded');
			
			},
			
			isWorking: function(el) {
				return el.hasClass('loaded') || el.hasClass('loading');
			},
			
			getSlides: function() {
				return slides || (slides = el.find('ul.lightslider').children('li')) && slides;
			},
			
			getActiveSlide: function() {
				return this.getSlides().filter(function() {
					return $(this).hasClass('active');
				});
			},
	};
	
	el
		.on('lightslider.loaded',function() {
			methods.loadActive();
		})
		.on('lightslider.afterSlide',function() {
			methods.loadActive();
		});
		
	el.hasClass('ls-loaded') && methods.loadActive();
		
	//no public API
	return {};
	
}(window[ns]));
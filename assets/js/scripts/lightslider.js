(function(context) {

	var 
		defaults = {
			loop: true,
			item: 1,
			controls: false, //light slider default
			pager: false, //light slider default
			mode: Modernizr.touch ? 'slide' : 'fade',
			slideMargin:0,
			onBeforeStart: function(el) { el.trigger('lightslider.beforeStart'); },
			onAfterSlide: function(el) { el.trigger('lightslider.afterSlide'); }
		};

	var methods = {
		
		applyLightSlider: function() {
			var 
				sliderInstance,
				el = $(this),
				ls = el.find('ul.lightslider'),
				args = $.extend({},defaults,ls.data());
				
				if(ls.data('ls')) { return; }
				
			sliderInstance = ls
				.lightSlider($.extend(args,{
					onSliderLoad: function() {
						el
							.addClass('ls-loaded')
							.addClass('mode-'+args.mode);
							
						ls.trigger('lightslider.loaded');
					}
				}));		
			
			ls.data('ls',sliderInstance);
			args.arrows && methods.buildControls(el,sliderInstance);
			args.links && methods.buildLinks(el,sliderInstance);
			
			el
				.on('lightslider.afterSlide lightSlider.loaded',function() {
					var el = $(this);
					
					el
						.find('div.ls-delegate-links')
						.children()
						.removeClass('selected')
						.eq(sliderInstance.getCurrentSlideCount()-1)
						.addClass('selected');
					
					el
						.find('div.ls-slider-links button')
						.removeClass('selected')
						.eq(sliderInstance.getCurrentSlideCount()-1)
						.addClass('selected');
				});
				
			el
				.find('div.ls-delegate-links')
				.children()
				.on('click',function() {
					sliderInstance.pause();
					sliderInstance.goToSlide($(this).index()+1);
				})
				.eq(sliderInstance.getCurrentSlideCount()-1)
				.addClass('selected');
			
		},
		
		buildControls: function(el,sliderInstance) {
			el
				.append('<div class="ls-slider-controls"><button class="prev"/><button class="next"/></div>')
				.on('click','div.ls-slider-controls button',function() {
					sliderInstance.pause();
					$(this).hasClass('next') ? sliderInstance.goToNextSlide() : sliderInstance.goToPrevSlide();
				});
			
		},
		
		buildLinks: function(el,sliderInstance) {
			var 
				ctrlString = '',
				currentPos = sliderInstance.getCurrentSlideCount()-1;
			for(var i = 0;i<sliderInstance.getTotalSlideCount();i++) {
				ctrlString += '<button class="'+(i == currentPos ? 'selected' : '')+'">'+i+'</button>';
			}
			
			el
				.append('<div class="ls-slider-links">'+ctrlString+'</div>')
				.on('click','div.ls-slider-links button',function() {
					sliderInstance.pause();
					sliderInstance.goToSlide($(this).index()+1);
					$(this)
						.addClass('selected')
						.siblings()
						.removeClass('selected')
				});
		}
		
	};
	
	$(document)
		.on('updateTemplate.lightslider',function() {
			$('.has-lightslider').each(function() { methods.applyLightSlider.apply(this); });
		}).trigger('updateTemplate.lightslider');
	
	$.extend(context,{
		lightslider: {
			applyLightSlider: function(el) {
				methods.applyLightSlider.apply(el);
			}
		}
	});

}(window[ns]));
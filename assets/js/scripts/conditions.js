(function(context) {

	var
		$req,
		title,
		$document = $(document),
		$html = $('html'),
		SHOW_CLASS = 'show-selected-condition',
		SELECTED_CLASS = 'selected',
		LOADING_CLASS = 'loading',
		LOADED_CLASS = 'loaded',
		$selectedCondition = $('div.condition-selected'),
		$items = $('div.conditions-grid .item'),
		$selector = $('div.conditions-selector select'),
		$content = $('div.condition-content'),
		methods = {
		
			clickHandler: function(el) {
				if(el.parent().hasClass(SELECTED_CLASS)) {
					this.hide();
					return;
				}
				
				
				$items.each(function() {
					$(this).parent().removeClass(SELECTED_CLASS);
				});
				
				el.parent().addClass(SELECTED_CLASS);
					
				this.load(el);
			},
			
			abort: function() {
				$req && $req.abort();
			},
			
			hide: function() {
			
				$items.each(function() {
					$(this).parent().removeClass(SELECTED_CLASS);
				});
			
				this.abort();
			
				$selectedCondition
					.removeClass(LOADING_CLASS)
					.removeClass(LOADED_CLASS);
					
				$content.html('');
					
				$html.removeClass(SHOW_CLASS);
			},
			
			load: function(el) {
				$html.addClass(SHOW_CLASS);
				
				title = el.data('title');
				
				this.abort();
				
				$selectedCondition
					.addClass(LOADING_CLASS)
					.removeClass(LOADED_CLASS);
				
				setTimeout(function() {
				
					$req = $.ajax({url: el.data('src')})
						.done(this.done.bind(this))
						.fail(this.failed.bind(this))
						.always(this.always.bind(this));
				
				}.bind(this),2000);

				
			},
			
			always: function() {
				$selectedCondition
					.removeClass(LOADING_CLASS)
					.addClass(LOADED_CLASS);
			},
			
			done: function(r,status,jqXHR) {
				if(!r || status !== 'success') {
					this.failed.apply(this,arguments);
					return;
				}
				
				$content.html(r);
			},
			
			failed: function(r,status,jqXHR) {
				if(status !== 'aborted') {
					$content.html('<span class="error">No '+title+' Conditions Found</span>')
				}
			},
			
			conditionClickHandler: function(el,e) {
			
				if(e.target.nodeName !== 'A') {
					el.toggleClass(SELECTED_CLASS);
					this.updateConditionCount();
				}
			},
			
			updateConditionCount: function() {
				var selected = $('div.condition-list li').filter(function() { return $(this).hasClass(SELECTED_CLASS); });
				$('div.conditions-book strong').html(selected.length);
			}
		
		};
		
	$items.on('click',function() { methods.clickHandler($(this)); });
	$document.on('click','div.condition-list li',function(e) { methods.conditionClickHandler($(this),e); });
	$selector.on('change',function(e) {
		var
			el = $(this),
			val = el.val(),
			option = el.find('option:selected');
			
		if(val) {
			methods.clickHandler($items.eq(option.index()-1));
		} else {
			methods.hide();
		}
	});

}(window[ns]));
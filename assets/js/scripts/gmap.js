(function(context) {

	var HTMLMarker = context.HTMLMarker;

	//Simple Embedded Google Map

	var loadGMap = function(cb) {	
		google.load('maps',3.15, {
			"base_domain":"google.ca","other_params":"sensor=false",
			"callback": cb
		});
	};

	var loadGoogleLoader = function(cb) {
		$.getScript('https://www.google.com/jsapi')
			.done(cb);
	};
	
	var Map = function(containerEl,options) {
		var self = this;
		
		google.maps.visualRefresh = true;
		
		this.canvas = containerEl;
		this.markers = [];
		var center = containerEl.data('center').split(',');
		
		this.canvas.on('mapLoad',function(e) { 
			containerEl.addClass('loaded'); 
			
			//fit to bounds on markers if necessary
			if(self.markers.length) {
				var 
					mapBounds = self.map.getBounds(),
					markerBounds = new google.maps.LatLngBounds();
					
				$.each(self.markers,function(i,marker) {
					markerBounds.extend(marker.getPosition());
				});
				
				if(!mapBounds.union(markerBounds).equals(mapBounds)) {
					self.map.fitBounds(markerBounds);
				}
				
			}
			
			
		});
		
		this.mapOptions = {
			center: new google.maps.LatLng(+center[0], +center[1]),
			zoom: +containerEl.data('zoom') || 15,
			streetViewControl:false,
			panControl:false,
			draggable: (!Modernizr.touch || $(window).outerWidth() > 600),
			mapTypeControlOptions:{
				style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			zoomControlOptions:{
				style:google.maps.ZoomControlStyle.SMALL
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
		this.map = new google.maps.Map(this.canvas[0],this.mapOptions);
		
		var 
			lastMarkerImage,
			lastMarkerIcon,
			markers = containerEl.data('markers');
		markers && markers.length && $.each(markers,function(i,markerObj) {
			var 
				markerIcon = null,
				center = markerObj.position.split(','),
				createMarker = function() {
					var opts = {
						position: new google.maps.LatLng(+center[0], +center[1]),
						map: self.map,
						title: markerObj.title
					};
					if(markerObj.htmlmarker) {
						self.markers.push(HTMLMarker.init($.extend(opts,{"class":markerObj.htmlmarker})));
					} else {
						self.markers.push(new google.maps.Marker($.extend(opts,{icon:markerIcon})));
					}
				};
			
			if(markerObj.image) {
				if(lastMarkerImage === markerObj.image) {
					markerIcon = lastMarkerIcon
					return;
				}
				
				$('<img/>')
					.on('load',function() {
						var
							width = this.width,
							height = this.height;
							
						//anchor is center
						markerIcon = { 
							anchor: new google.maps.Point(this.width/2,this.height/2), 
							url: templateJS.templateURL+markerObj.image
						};
						
						lastMarkerImage = markerObj.image;
						lastMarkerIcon = markerIcon;
						createMarker();
					})
					.attr('src',templateJS.templateURL+markerObj.image);
					return;
			}
			
			createMarker();

		});
		
		this.convertListeners();
		
	};

	
	Map.prototype.convertListeners = function() {
	
		this.mapListeners = {};
	
			var 
				self = this,
				ns = google.maps.event,
				loadListener = ns.addListener(self.map,'tilesloaded',function() { 
					ns.removeListener(loadListener);
					self.canvas.trigger('mapLoad',[self]);
					self.mapListeners.resizeListener = self.mapListeners.resizeListener || ns.addListener(self.map,'resize',function() { self.canvas.trigger('mapResized'); });
					self.mapListeners.idleListener = self.mapListeners.idleListener || ns.addListener(self.map,'idle',function() { 
							var 
								args = {},
								center = self.map.getCenter();
								
								args.lat = center.lat();
								args.lng = center.lng();
								args.zoom = self.map.getZoom();
							
							self.canvas.trigger('mapMoved',[args]); 
					});
					self.mapListeners.zoomListener = self.mapListeners.zoomListener || ns.addListener(self.map,'zoom_changed',function() { self.canvas.trigger('mapZoomed',[self.map.getZoom()]); });
				});
				self.mapListeners.mapTypeListener = self.mapListeners.mapTypeListener || ns.addListener(self.map,'maptypeid_changed',function() { self.canvas.trigger('mapTypeChanged'); });
				
			self.canvas.on('doResizeMap',function() { ns.trigger(self.map,'resize'); });
	
	};

	var 
		hasRequestedGoogle = false,
		googleDefer = $.Deferred(),
		googleRequestComplete = googleDefer.promise();
		
	$('div.map').each(function() {
		var el = this;
		
		var cb = function() {
			var map = new Map($(this));
			window.maps = window.maps || [];
			window.maps.push(map);
			
			$(this).data('map',map);
		}
		
		if(!hasRequestedGoogle) {
			hasRequestedGoogle = true;
		} else {
			return googleRequestComplete.done(function() {
				cb.apply(el);
			});
		}
		
		if(window.google === undefined || window.google.maps === undefined) { 
		
			window.createMap = function() { 
				googleDefer.resolve();
				cb.apply(el); 
			};
			
			return $.getScript('https://maps.googleapis.com/maps/api/js?sensor=false&callback=createMap');
		}
		
		if(window.google === undefined) { return loadGoogleLoader(function() { loadGMap(function() { cb.apply(el); }); }); }
		
		if(window.google.maps === undefined) { return loadGMap(function() { cb.apply(el); }); }
		
		cb.apply(el);
		
	});	
	
	return {};
	
}(window[ns]));
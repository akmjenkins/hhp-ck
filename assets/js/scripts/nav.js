(function(context) {

	var 
		scrollDebounce = context.debounce(),
		resizeDebounce = context.debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$nav = $('nav'),
		$navWrap  =$('div.nav'),
		$body = $('body'),
		$pageWrapper = $('div.page-wrapper'),
		startingNavHeight = $navWrap.height(),
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav',
		COLLAPSE_NAV_AT = 750;

	var methods = {
	
		checkShowSmallNav: function() {
			
			if(window.innerWidth > COLLAPSE_NAV_AT && $window.scrollTop() > startingNavHeight) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
			$nav.find('div.swipe').removeClass('rebuilt');
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.isShowingNav() && methods.showNav(false);
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27 && methods.isShowingNav()) {
				methods.showNav(false);
				return false;
			}
		})

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(Modernizr.ios) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
		
	//when necessary, rebuild dropdown swipers
	$nav
		.on('mouseenter','>ul>li',function(e) {
			var
				el  =$(this),
				dd = el.children('div'),
				swiperEl = dd.find('div.swipe'),
				swiper = swiperEl.data('swipe');
			
				//rebuild
				if(swiper && !swiperEl.hasClass('rebuilt')) {
					swiper.setup();
					swiperEl.addClass('rebuilt');
				}
				
		});
		

	//no public API
	return {};

}(window[ns]));
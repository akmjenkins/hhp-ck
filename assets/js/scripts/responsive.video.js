(function(context) {

	var 
		$window = $(window),
		methods = {
		
			getEls: function() {
				return $('iframe.video,iframe[src*="youtube"],iframe[src*="vimeo"]').filter(function() { return $(this).parent().hasClass('video-container'); });
			},
			
			update: function() {
				this.getEls().each(function() { 
					$(this).removeAttr('width').removeAttr('height').wrap('<div class="video-container"/>') 
				});
			}
		
		};
	
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.video'); or just $(document).trigger('updateTemplate');
	$window.on('load updateTemplate.responsivevideo',function(e) {
		methods.update();
	})
	
	
	//initial setup
	methods.update();

}(window[ns]));